﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Suggestion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cb_topics = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_name = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_suggestion = New System.Windows.Forms.RichTextBox()
        Me.btn_close = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(473, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Thank you for making a suggestion; please enter items below where valid:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Concerns:"
        '
        'cb_topics
        '
        Me.cb_topics.FormattingEnabled = True
        Me.cb_topics.Items.AddRange(New Object() {"Visuals", "Commands", "Features", "Other"})
        Me.cb_topics.Location = New System.Drawing.Point(90, 60)
        Me.cb_topics.Name = "cb_topics"
        Me.cb_topics.Size = New System.Drawing.Size(121, 24)
        Me.cb_topics.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(163, 17)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Please enter your name:"
        '
        'txt_name
        '
        Me.txt_name.Location = New System.Drawing.Point(181, 94)
        Me.txt_name.Name = "txt_name"
        Me.txt_name.Size = New System.Drawing.Size(193, 22)
        Me.txt_name.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(12, 126)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(681, 23)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Please enter all information regarding your suggestion below:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_suggestion
        '
        Me.btn_suggestion.Location = New System.Drawing.Point(12, 152)
        Me.btn_suggestion.Name = "btn_suggestion"
        Me.btn_suggestion.Size = New System.Drawing.Size(681, 291)
        Me.btn_suggestion.TabIndex = 6
        Me.btn_suggestion.Text = ""
        '
        'btn_close
        '
        Me.btn_close.Location = New System.Drawing.Point(611, 12)
        Me.btn_close.Name = "btn_close"
        Me.btn_close.Size = New System.Drawing.Size(82, 40)
        Me.btn_close.TabIndex = 7
        Me.btn_close.Text = "Close"
        Me.btn_close.UseVisualStyleBackColor = True
        '
        'Suggestion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(705, 455)
        Me.Controls.Add(Me.btn_close)
        Me.Controls.Add(Me.btn_suggestion)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txt_name)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cb_topics)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Suggestion"
        Me.Text = "Suggestion"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cb_topics As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt_name As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents btn_suggestion As RichTextBox
    Friend WithEvents btn_close As Button
End Class
