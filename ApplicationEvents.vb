﻿Imports System.Reflection

Namespace My
    ' The following events are available for MyApplication:
    ' Startup: Raised when the application starts, before the startup form is created.
    ' Shutdown: Raised after all application forms are closed.  This event is not raised if the application terminates abnormally.
    ' UnhandledException: Raised if the application encounters an unhandled exception.
    ' StartupNextInstance: Raised when launching a single-instance application and the application is already active. 
    ' NetworkAvailabilityChanged: Raised when the network connection is connected or disconnected.
    Partial Friend Class MyApplication
        Private Sub AppStart(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup
            AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf ResolveAssemblies
        End Sub

        Public Function ResolveAssemblies(sender As Object, args As System.ResolveEventArgs) As System.Reflection.Assembly
            Dim ressourceName = New AssemblyName(args.Name).Name + ".dll"
            Using stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(ressourceName)
                Dim assemblyData(CInt(stream.Length)) As Byte
                stream.Read(assemblyData, 0, assemblyData.Length)
                Return Assembly.Load(assemblyData)
            End Using
        End Function
    End Class
End Namespace
