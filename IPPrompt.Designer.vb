﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class IPPrompt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdEnter = New System.Windows.Forms.Button()
        Me.lblmsg = New System.Windows.Forms.Label()
        Me.txtIP_1 = New System.Windows.Forms.TextBox()
        Me.txtIP_2 = New System.Windows.Forms.TextBox()
        Me.txtIP_3 = New System.Windows.Forms.TextBox()
        Me.txtIP_4 = New System.Windows.Forms.TextBox()
        Me.lblIP = New System.Windows.Forms.Label()
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.dgvMasterlist = New System.Windows.Forms.DataGridView()
        Me.lblMasterlist = New System.Windows.Forms.Label()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvMasterlist, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdEnter
        '
        Me.cmdEnter.Location = New System.Drawing.Point(125, 531)
        Me.cmdEnter.Name = "cmdEnter"
        Me.cmdEnter.Size = New System.Drawing.Size(270, 53)
        Me.cmdEnter.TabIndex = 6
        Me.cmdEnter.Text = "Enter"
        Me.cmdEnter.UseVisualStyleBackColor = True
        '
        'lblmsg
        '
        Me.lblmsg.Location = New System.Drawing.Point(50, 461)
        Me.lblmsg.Name = "lblmsg"
        Me.lblmsg.Size = New System.Drawing.Size(285, 64)
        Me.lblmsg.TabIndex = 5
        Me.lblmsg.Text = "Please enter the IP Address" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "    First      .   Second  .    Third     .   Fourth" &
    "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'txtIP_1
        '
        Me.txtIP_1.Location = New System.Drawing.Point(53, 503)
        Me.txtIP_1.MaxLength = 3
        Me.txtIP_1.Name = "txtIP_1"
        Me.txtIP_1.Size = New System.Drawing.Size(66, 22)
        Me.txtIP_1.TabIndex = 4
        '
        'txtIP_2
        '
        Me.txtIP_2.Location = New System.Drawing.Point(125, 503)
        Me.txtIP_2.MaxLength = 3
        Me.txtIP_2.Name = "txtIP_2"
        Me.txtIP_2.Size = New System.Drawing.Size(66, 22)
        Me.txtIP_2.TabIndex = 7
        '
        'txtIP_3
        '
        Me.txtIP_3.Location = New System.Drawing.Point(197, 503)
        Me.txtIP_3.MaxLength = 3
        Me.txtIP_3.Name = "txtIP_3"
        Me.txtIP_3.Size = New System.Drawing.Size(66, 22)
        Me.txtIP_3.TabIndex = 8
        '
        'txtIP_4
        '
        Me.txtIP_4.Location = New System.Drawing.Point(269, 503)
        Me.txtIP_4.MaxLength = 3
        Me.txtIP_4.Name = "txtIP_4"
        Me.txtIP_4.Size = New System.Drawing.Size(66, 22)
        Me.txtIP_4.TabIndex = 9
        '
        'lblIP
        '
        Me.lblIP.AutoSize = True
        Me.lblIP.Location = New System.Drawing.Point(50, 561)
        Me.lblIP.Name = "lblIP"
        Me.lblIP.Size = New System.Drawing.Size(68, 17)
        Me.lblIP.TabIndex = 10
        Me.lblIP.Text = "127.0.0.1"
        '
        'btnHelp
        '
        Me.btnHelp.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnHelp.ForeColor = System.Drawing.Color.Maroon
        Me.btnHelp.Location = New System.Drawing.Point(306, 441)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(72, 30)
        Me.btnHelp.TabIndex = 11
        Me.btnHelp.Text = "Help!"
        Me.btnHelp.UseVisualStyleBackColor = False
        '
        'dgvMasterlist
        '
        Me.dgvMasterlist.AllowUserToAddRows = False
        Me.dgvMasterlist.AllowUserToDeleteRows = False
        Me.dgvMasterlist.AllowUserToResizeColumns = False
        Me.dgvMasterlist.AllowUserToResizeRows = False
        Me.dgvMasterlist.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvMasterlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMasterlist.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2})
        Me.dgvMasterlist.Location = New System.Drawing.Point(12, 29)
        Me.dgvMasterlist.Name = "dgvMasterlist"
        Me.dgvMasterlist.ReadOnly = True
        Me.dgvMasterlist.RowHeadersVisible = False
        Me.dgvMasterlist.RowTemplate.Height = 24
        Me.dgvMasterlist.Size = New System.Drawing.Size(383, 406)
        Me.dgvMasterlist.TabIndex = 12
        '
        'lblMasterlist
        '
        Me.lblMasterlist.AutoSize = True
        Me.lblMasterlist.Location = New System.Drawing.Point(12, 9)
        Me.lblMasterlist.Name = "lblMasterlist"
        Me.lblMasterlist.Size = New System.Drawing.Size(51, 17)
        Me.lblMasterlist.TabIndex = 13
        Me.lblMasterlist.Text = "Label1"
        '
        'Column1
        '
        Me.Column1.HeaderText = "Server Name"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.HeaderText = "Server IP"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'IPPrompt
        '
        Me.AcceptButton = Me.cmdEnter
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(407, 593)
        Me.Controls.Add(Me.lblMasterlist)
        Me.Controls.Add(Me.dgvMasterlist)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.lblIP)
        Me.Controls.Add(Me.txtIP_4)
        Me.Controls.Add(Me.txtIP_3)
        Me.Controls.Add(Me.txtIP_2)
        Me.Controls.Add(Me.cmdEnter)
        Me.Controls.Add(Me.txtIP_1)
        Me.Controls.Add(Me.lblmsg)
        Me.Name = "IPPrompt"
        Me.Text = "Enter the IP Address"
        CType(Me.dgvMasterlist, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents cmdEnter As Button
    Private WithEvents lblmsg As Label
    Private WithEvents txtIP_1 As TextBox
    Private WithEvents txtIP_2 As TextBox
    Private WithEvents txtIP_3 As TextBox
    Private WithEvents txtIP_4 As TextBox
    Friend WithEvents lblIP As Label
    Friend WithEvents btnHelp As Button
    Friend WithEvents dgvMasterlist As DataGridView
    Friend WithEvents lblMasterlist As Label
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
End Class
