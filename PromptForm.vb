﻿Public Class PromptForm
    Private msg As String
    Private password As Boolean
    Public Returned As String
    Private Sub Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblmsg.Text = msg
        If password Then
            txt_Entry.PasswordChar = "*"
        End If
    End Sub

    Public Sub SetMessage(mainMessage As String, Optional title As String = "Client input prompt", Optional _password As Boolean = False)
        msg = mainMessage.Trim() ' removes blanks that are used for [double click] messages
        password = _password
        Me.Text = title
    End Sub

    Private Sub cmdEnter_Click(sender As Object, e As EventArgs) Handles cmdEnter.Click
        If String.IsNullOrEmpty(txt_Entry.Text) Then
            MsgBox("You must enter an input.")
            Return
        End If

        Returned = txt_Entry.Text
        Me.Close()
    End Sub
End Class