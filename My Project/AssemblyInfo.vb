﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Chat Program Client, Alex.")>
<Assembly: AssemblyDescription("Alexs Chat Program Client")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("ChatProg")>
<Assembly: AssemblyCopyright("Copyright ©  2008")> 
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(True)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("b8cb3308-fc7b-4cfc-bc9d-9196123479a8")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.4.6.1")>
<Assembly: AssemblyFileVersion("2.4.6.1")>
