﻿Public Class Suggestion
    Public ReadOnly Property Concerns As String
        Get
            Return cb_topics.SelectedItem
        End Get
    End Property
    Public ReadOnly Property UserName As String
        Get
            Return txt_name.Text
        End Get
    End Property
    Public ReadOnly Property Suggestion_Main As String
        Get
            Return btn_suggestion.Text
        End Get
    End Property

    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        Me.Close()
    End Sub

    Private Sub Suggestion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cb_topics.SelectedIndex = 0
        txt_name.Text = MainForm.txt_Name.Text
    End Sub
End Class