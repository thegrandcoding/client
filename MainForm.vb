﻿Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Reflection
Imports System.Text
Public Class MainForm
    Dim clientSocket As New System.Net.Sockets.TcpClient()
    Dim serverStream As NetworkStream
    Dim readData As String
    Dim crit As Boolean
    Dim infiniteCounter As Integer
    Dim lastMessage As String
    Dim UserStruct As UserClient
    Dim connectIP As String = "127.0.0.1"
    Friend _Log As LogHandle
    Private _isFocused As Boolean = True
    Public Notify As Boolean = True
    Public DoLog As Boolean = True
    Public ChatMode As String = "Public"
    Public CanReport As Boolean = False
    Public ShouldReport As Boolean = True
    Private secondLastMessage As String = ""
    Public ReadOnly Property ChatModeCommand As String
        Get
            If ChatMode = "Public" Then
                Return ""
            ElseIf ChatMode = "Admin Chat" Then
                Return "/a"
            ElseIf ChatMode = "Manager Chat" Then
                Return "/m"
            End If
            Return ""
        End Get
    End Property
    Public Const MainReg As String = "HKEY_CURRENT_USER\Alex_ChatProgram"
    Public Structure UserClient
        Public Name As String
        Public Serial As String
        Public Checksum As String ' random int
        Public _LoggedIn As Boolean
        Public Rank As Integer
        Public Property LoggedIn As Boolean
            Get
                If Connected = False Then _LoggedIn = False
                Return _LoggedIn
            End Get
            Set(value As Boolean)
                _LoggedIn = value
            End Set
        End Property
        Public ReadOnly Property Connected As Boolean
            Get
                Return MainForm.clientSocket.Connected
            End Get
        End Property
    End Structure

    Public Function GetLatestVersion(url As String) As Version
        Try
            Dim myReq As HttpWebRequest = WebRequest.Create(url)
            Dim response As HttpWebResponse = myReq.GetResponse()
            Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
            Dim strResponse As String = resStream.ReadToEnd().ToString()
            Dim location As Integer = strResponse.IndexOf("<Assembly: AssemblyFileVersion(")
            If location >= 0 Then
                Dim getVersion As String = strResponse.Substring(location + "<Assembly: AssemblyFileVersion(".Length + 1, 7)
                Dim latest As Version = New Version(getVersion)
                Return latest
            Else
                Return New Version("0.0.0.0")
            End If
        Catch ex As Exception
            _Log.LogError("Error occured: " + ex.ToString())
            _Log.LogError("Stack trace: " + ex.StackTrace)
        End Try
        Return New Version("0.0.0.0")
    End Function

    Private CurrentlySending As Boolean = False
    Public Sub WaitToSend()
        While CurrentlySending = True
            Threading.Thread.Sleep(50)
        End While
        CurrentlySending = True
    End Sub

    Private Function SendMessage(message As String, Optional dontReplace As Boolean = False) As String
        If clientSocket.Connected = False Or String.IsNullOrEmpty(message) Then Return ""
        If dontReplace = False Then message = message.Replace("&", "&AndSymbol&") ' disallow & symbol, as it is used or oclor.
        message = message.Replace("$", "&Dollar&")
        If message.Substring(message.Length - 1, 1) = "$" Then
            readData = "&CYAN&<< Error: dollar sign cannot be at end of message"
            msg()
        End If
        If message.Length > 256 Then
            readData = "&CYAN&<< Error: message too long."
            msg()
            Return ""
        End If
        WaitToSend()
        _Log.LogMsg("<<: " + StripColors(message))
        Dim outStream As Byte() = System.Text.Encoding.UTF8.GetBytes("%" + message + "$$$")
        serverStream.Write(outStream, 0, outStream.Length)
        serverStream.Flush()
        CurrentlySending = False
        Return ""
    End Function
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSend.Click
        Dim msgToSend As String = txt_send.Text
        If String.IsNullOrEmpty(msgToSend) Then Return
        If Not msgToSend.Substring(0, 1) = "/" Then
            ' message to chat mode
            If Not String.IsNullOrEmpty(ChatModeCommand) Then
                ' does have chatmode
                msgToSend = ChatModeCommand + " " + msgToSend ' add command chatmode
            End If
        End If
        HasSentIsTyping = False
        SendMessage(msgToSend)
        lastMessage = msgToSend
        txt_send.Text = ""
    End Sub

    Private ColorConvert As New Dictionary(Of String, Color)

    Private Sub HandleColors()
        ' loop thru each item in listbox, replace color to the listbox foreground
        For Each item As ListViewItem In lb_Chat.Items
            If Not item.Text.Contains("&") Then Continue For
            For Each col As String In ColorConvert.Keys
                Dim indexOfColor = item.Text.IndexOf(col)
                If indexOfColor < 0 Then Continue For
                Dim txt As String = item.Text
                While txt.Contains(vbNullChar)
                    txt = txt.Remove(vbNullChar)
                End While
                txt = txt.Replace(col, "")
                item.Text = txt
                item.ForeColor = ColorConvert(col)
                If item.Text.Contains(MentionString) Or item.Text.Contains("PM from") Or item.Text.Contains("@everyone") Then
                    item.BackColor = Color.Yellow
                Else
                    item.BackColor = Color.White
                End If
            Next
        Next
    End Sub
    Private shouldFlash As Boolean = False
    Public Sub msg(Optional message As String = "", Optional critical As Boolean = Nothing)
        If Me.InvokeRequired Then
            If message IsNot "" Then
                readData = message
            End If
            If critical <> Nothing Then
                crit = critical
            End If
            ' cross-thread support for arguements
            Try
                Me.Invoke(Sub() msg())
            Catch ex As Exception
            End Try
            Return
        Else
            If message = "" Then message = readData
            If critical = Nothing Then critical = crit
            If critical = True Then
                lb_Chat.Items.Add("&CYAN&<< " + message)
                _Log.LogMsg(StripColors("&CYAN&<< " + message))
            Else
                lb_Chat.Items.Add(message)
                _Log.LogMsg(StripColors(message))
                ' now we check if the user knows of this change
                If Me.WindowState = 1 Or (_isFocused = False) Then ' Window is minimised
                    If Notify = True Then
                        shouldFlash = True
                    Else
                        shouldFlash = False
                    End If
                    If message.Contains(MentionString) Or message.Contains("@everyone") Or message.Contains("PM from") Then
                        shouldFlash = True
                        ' override notifcation disable if they are directly mentioned
                        ' @everyone can only be used by server Managers, so that is good.
                    End If
                    If shouldFlash = True Then
                        Try
                            My.Computer.Audio.PlaySystemSound(System.Media.SystemSounds.Hand)
                        Catch ex As Exception
                            _Log.LogError("Error during notif play: " + ex.ToString())
                        End Try
                    End If
                End If
            End If

            lb_Chat.EnsureVisible(lb_Chat.Items.Count - 1)
            HandleColors()
            If lb_Chat.Items.Item(lb_Chat.Items.Count - 1).Text.Contains(">> Logged in successfully") Then
                UserStruct.LoggedIn = True
                If lb_Chat.Items.Item(lb_Chat.Items.Count - 1).Text.Contains("rank as: Moderator") Then
                    UserStruct.Rank = 1
                    Commands.Clear()
                ElseIf lb_Chat.Items.Item(lb_Chat.Items.Count - 1).Text.Contains("rank as: Admin") Then
                    UserStruct.Rank = 2
                    Commands.Clear()
                ElseIf lb_Chat.Items.Item(lb_Chat.Items.Count - 1).Text.Contains("rank as: Manager") Then
                    UserStruct.Rank = 3
                    Commands.Clear() ' Clearing ensures that the admin commands are added.
                Else
                    UserStruct.Rank = 0
                End If
            End If
            If lb_Chat.Items.Item(lb_Chat.Items.Count - 1).Text.Contains(">> Registered successfully") Then
                UserStruct.LoggedIn = True
                UserStruct.Rank = 0
            End If
        End If
    End Sub
    Public HasConnected As Boolean = False
    Public IsTryingToConnect As Boolean = False
    Dim ctThread As Threading.Thread
    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        If String.IsNullOrEmpty(txt_Name.Text) Then
            MessageBox.Show("You must enter a name!")
            Return
        End If
        IsTryingToConnect = True
        ReConnectTimer.Start()
        SetOption("DefaultName", txt_Name.Text)
        SetOption("LastConnection", DateTime.Now())
        lastMessage = ""
        'lb_Chat.Items.Clear()
        _Log.SetUser(txt_Name.Text)
        _Log.SetSerial(UserStruct.Serial)
        clientSocket = New System.Net.Sockets.TcpClient()
        Try
            ReConnectTimer.Start()
            crit = True
            readData = "&CYAN&Attempting to connect to server... (" + DateTime.Now().ToLongTimeString + ")"
            msg()
            clientSocket.Connect(connectIP, 55001)
            readData = "&CYAN&Connected."
            msg()
            HasConnected = True
            crit = False
            serverStream = clientSocket.GetStream()
            clientSocket.ReceiveBufferSize = 16348
        Catch ex As SocketException
            readData = "&RED&Connection failed. Perhaps the server is offline?"
            msg()
            readData = ex.ToString()
            msg()
            _Log.SaveLog("Disconnect")
            Return
        Finally
            IsTryingToConnect = False
        End Try
        Dim outStream As Byte() = System.Text.Encoding.UTF8.GetBytes(txt_Name.Text + "&" + UserStruct.Serial + "&" + UserStruct.Checksum + "&" + versionNumber.ToString + "$$$")
        serverStream.Write(outStream, 0, outStream.Length)
        serverStream.Flush()
        cmdConnect.Enabled = Not clientSocket.Connected
        ctThread = New Threading.Thread(AddressOf getMessage)
        ctThread.Name = "Recieve message"
        ctThread.Start()
        ' re-enable buttons if it worked.
        txt_send.Enabled = clientSocket.Connected
        cmdSend.Enabled = clientSocket.Connected
    End Sub

    Public Sub CheckIfShouldReconnect()
        UpdateSecondLastMsg()
        Threading.Thread.Sleep(2)
        If Not String.IsNullOrEmpty(secondLastMessage) Then
            If secondLastMessage.Contains(UserStruct.Name + " was") Then
                Try
                    CloseClient("1 This should not get through")
                Catch ex As Exception
                    _Log.LogWarn(ex.ToString())
                End Try
                ReConnectTimer.Stop()
                _Log.LogMsg("Stopping reconnect on possibility of punishment")
            ElseIf secondLastMessage.Contains("You are ") Then
                Try
                    CloseClient("2 This should not get through")
                Catch ex As Exception
                    _Log.LogWarn(ex.ToString())
                End Try
                ReConnectTimer.Stop()
                _Log.LogMsg("Stopping reconnect on possibility of server-intended disconnect (banned?)")
            End If
        End If
    End Sub

    Private Sub SetCurTyping(str As String)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() SetCurTyping(str)) ' cross-thread support for arguements
            Return
        Else
            lbl_CurTyping.Text = str
        End If
    End Sub

    Public ReadOnly Property MentionString As String
        Get
            Return "@" + txt_Name.Text
        End Get
    End Property

    Public Function FirstXChars(str As String, length As Integer) As String
        Dim returned As String = ""
        Try
            returned = str.Substring(0, length)
        Catch ex As Exception
        End Try
        Return returned
    End Function

    Private Sub getMessage()
        For infiniteCounter = 1 To 2
            Try
                infiniteCounter = 1
                serverStream = clientSocket.GetStream()
                Dim buffSize As Integer
                Dim inStream(65535) As Byte
                buffSize = clientSocket.ReceiveBufferSize
                serverStream.Read(inStream, 0, buffSize)
                Dim returndata As String = Encoding.UTF8.GetString(inStream)
                returndata = returndata.Replace(vbNullChar, String.Empty)
                'returndata = returndata.Substring(0, returndata.IndexOf("$$$"))
                readData = "" + returndata
                Dim rdSplit As String() = readData.Split("%")
                For Each _msg As String In rdSplit
                    If String.IsNullOrEmpty(_msg) Then Continue For
                    _msg = _msg.Substring(0, _msg.IndexOf("$$$"))
                    _msg = _msg.Replace("&Dollar&", "$").Replace("&AndSymbol&", "&")
                    _Log.LogMsg("FromServer: " + _msg)
                    readData = _msg
                    If _msg.Substring(0, 1) = "/" Then
                        _msg = _msg.Remove(0, 1)
                        Dim split As New List(Of String)
                        split.AddRange(_msg.Split(" "))
                        Dim cmd As String = split.Item(0)
                        split.RemoveAt(0)
                        HandleServerCommand(cmd, split)
                        Continue For
                    End If
                    If _msg.Contains("&TYPING&:") Then
                        _msg = _msg.Remove(0, "&TYPING&:".Length)
                        Dim users As String() = _msg.Split(",")
                        If _msg.Length = 0 Then
                            SetCurTyping("No one is currently typing.")
                        Else
                            Dim str As String = "Currently typing: "
                            For Each item As String In users
                                str += item + ", "
                            Next
                            str = str.Remove(str.Length - 2, 2)
                            SetCurTyping(str)
                        End If
                        _msg = ""
                        Continue For
                    End If
                    Dim firstXchara As String = FirstXChars(StripColors(_msg), (txt_Name.Text + ":/").ToString.Length)
                    If firstXchara = (txt_Name.Text + ":/") Then
                        ' command of player
                        _Log.LogMsg("Msg Passed: " & _msg)
                        Continue For
                    End If
                    If _msg.Contains("&IGNORE&") Then
                        Continue For
                    End If
                    msg()
                Next
                Continue For
            Catch ex As System.IO.IOException
                readData = "&L_RED&Connection to the server was lost. " & MentionString
                msg()
                CheckIfShouldReconnect()
                CloseClient("3 This should not get through")
                _Log.LogWarn(ex.ToString())
                _Log.SaveLog("Disconnect")
                Exit For
            Catch ex As Exception
                readData = "&L_RED&<< Connection lost " & MentionString
                msg()
                CheckIfShouldReconnect()
                CloseClient("4 This should not get through")
                _Log.LogWarn("Possible error, message: " + ex.ToString())
                _Log.SaveLog("Disconnect/Error")
                Exit For
            End Try
        Next
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            SendMessage("/quit [close button]")
        Catch ex As Exception
            ' do nothing, just close.
        End Try
        _Log.LogMsg("Close via close button")
        Application.Exit()
    End Sub

    Public Function EncryptSHA256Managed(ByVal ClearString As String) As String
        Dim uEncode As New UnicodeEncoding()
        Dim bytClearString() As Byte = uEncode.GetBytes(ClearString)
        Dim sha As New _
        System.Security.Cryptography.SHA256Managed()
        Dim hash() As Byte = sha.ComputeHash(bytClearString)
        Return Convert.ToBase64String(hash)
    End Function

    Private rand As New Random
    Private Function GenerateRandom64Digit() As String
        Dim str As String = ""
        For i As Integer = 0 To 64
            str += rand.Next(0, 9).ToString()
        Next
        Return str
    End Function

    Friend Sub HandleRegistryStuff()
        Dim readValue As String = My.Computer.Registry.GetValue(MainReg + "\Client", "SerialKey", Nothing)
        Dim tobeSerial As String = ""
        Dim checkSumInt As String = My.Computer.Registry.GetValue(MainReg + "\Client", "CheckSum", Nothing)
        If readValue Is Nothing Then
            Try
                Dim numberRand As String = GenerateRandom64Digit()
                tobeSerial = EncryptSHA256Managed(numberRand)
                _Log.LogMsg("Generating checksum.")
                Dim randInt As String = "0"
                Dim testCheckSumSerial As String = EncryptSHA256Managed(tobeSerial + randInt)
                While Not testCheckSumSerial.Substring(0, 2) = "00"
                    _Log.LogMsg("Checksum(" + randInt + "): " + testCheckSumSerial.Substring(0, 2))
                    randInt = (Convert.ToInt32(randInt) + 1).ToString
                    testCheckSumSerial = EncryptSHA256Managed(tobeSerial + randInt)
                    If randInt > 2000000 Then Throw New Exception("Timeout.")
                End While
                _Log.LogMsg("Checksum:  " + randInt + ", serial: " + testCheckSumSerial)
                checkSumInt = randInt
                My.Computer.Registry.CurrentUser.CreateSubKey("Alex_ChatProgram")
                My.Computer.Registry.SetValue(MainReg + "\Client", "SerialKey", tobeSerial)
                My.Computer.Registry.SetValue(MainReg + "\Client", "CheckSum", randInt)
                readValue = My.Computer.Registry.GetValue(MainReg + "\Client", "SerialKey", Nothing)
            Catch ex As System.Security.SecurityException
                _Log.LogError("Access denied to registry: " + ex.ToString())
                readValue = tobeSerial
            Catch ex As Exception
                _Log.LogError("Unknown error in registry: " + ex.ToString())
                readValue = tobeSerial
            End Try
        End If
        Dim readNotify As Boolean = My.Computer.Registry.GetValue(MainReg + "\Client", "Notifications", True)
        Notify = readNotify
        UserStruct.Serial = readValue
        UserStruct.Checksum = checkSumInt
    End Sub

    Private Sub IPHandle()
        Dim ipSelect As New IPPrompt()
        ipSelect.ShowDialog()
        If Not String.IsNullOrEmpty(ipSelect.SelectedIP) Then
            connectIP = ipSelect.SelectedIP
        End If
        ipSelect.Dispose()
    End Sub

    Private Function StripColors(str As String) As String
        For Each clr As String In ColorConvert.Keys
            While str.Contains(clr)
                str = str.Replace(clr, "")
            End While
        Next
        Return str
    End Function

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        OriginalSize = txtToolTip.Size
        OriginalLocation = txtToolTip.Location
        txt_Name.Text = GetOption("DefaultName", "")
        versionNumber = Assembly.GetExecutingAssembly().GetName().Version
        _Log = New LogHandle(IO.Directory.GetCurrentDirectory + "\logs\", "Not set", "Not set", versionNumber)
        ColorConvert.Add("&PURPLE&", Color.Purple)
        ColorConvert.Add("&L_PURPLE&", Color.MediumPurple)
        ColorConvert.Add("&BLACK&", Color.Black)
        ColorConvert.Add("&L_RED&", Color.Red)
        ColorConvert.Add("&BLUE&", Color.Blue)
        ColorConvert.Add("&L_ORANGE&", Color.Orange)
        ColorConvert.Add("&CYAN&", Color.Cyan)
        ColorConvert.Add("&RED&", Color.DarkRed)
        ColorConvert.Add("&ORANGE&", Color.DarkOrange)
        lb_Chat.Columns.Item(0).Width = lb_Chat.Width - 10
        txt_send.Enabled = False
        cmdSend.Enabled = False
        IPHandle()
        UserStruct = New UserClient With {.Name = txt_Name.Text, .Rank = -1}
        HandleRegistryStuff() 'gets the serial for  the user.
        Dim latestClient As Version = GetLatestVersion("https://bitbucket.org/thegrandcoding/client/raw/HEAD/My%20Project/AssemblyInfo.vb")
        If versionNumber.CompareTo(latestClient) >= 0 Then
            _Log.LogMsg("Client is uptodate")
            If File.Exists("newClient.exe") Then
                If Not IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath).ToString = "newClient" Then
                    File.Delete("newClient.exe") ' no need to keep an outdated client.
                End If
            End If
        Else
            _Log.LogWarn("Client is out-of-date, latest: " + latestClient.ToString)
            MsgBox("Warning:" + vbCrLf + "Your client is out of date" + vbCrLf + "Your version: " + versionNumber.ToString + vbCrLf + "New version: " + latestClient.ToString() & vbCrLf & vbCrLf & "This client will now attempt to download the latest version..")
            ' Auto-download latest version (will be toggleable according to registry)
            ' TODO: toggle via registry of whether this happens.
            Try
                Dim web_Download As New WebClient
                If File.Exists("newClient.exe") Then
                    Dim dlVersion As Version = New Version(FileVersionInfo.GetVersionInfo("newClient.exe").FileVersion)
                    If Not dlVersion = latestClient Then
                        File.Delete("newClient.exe") ' not the latest version
                    End If
                Else
                    web_Download.DownloadFile("https://bitbucket.org/thegrandcoding/client/raw/HEAD/bin/Debug/client.exe", "newClient.exe")
                End If
                MsgBox("New client has been downloaded. Checking version...") ' we dont tell them of the old version.
                Dim downloadVersion As Version = New Version(FileVersionInfo.GetVersionInfo("newClient.exe").FileVersion)
                If downloadVersion = latestClient Then
                    MsgBox("Version is valid. Running new client now.. this client will close.")
                    Process.Start("newClient.exe")
                    Threading.Thread.Sleep(5)
                    _Log.SaveLog("Closing: new client downloaded & ran")
                    End
                Else
                    _Log.LogError("Downloaded client was of incorrect version, what?")
                    MsgBox("Attempts to download the new version have failed." & vbCrLf & "Please download manually.")
                End If
            Catch ex As Exception
                MsgBox(ex.ToString())
                _Log.LogError(ex.ToString)
            End Try
        End If
        If IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath).ToString = "newClient" Then
            MsgBox("WARNING:" & vbCrLf & "It is advised that you download the complete Visual Studio project of the client." & vbCrLf & "You may ignore this message.")
        End If
        Dim shouldLog As Boolean = My.Computer.Registry.GetValue(MainReg + "\Client", "Logging", True)
        My.Computer.Registry.SetValue(MainReg + "\Client", "Logging", shouldLog) 'will create it as True if it doenst exist
        DoLog = shouldLog

        ' Handle the new DLL required..
        If File.Exists("BugReport.dll") Then
            _Log.LogMsg("Bug reporting is able.")
            CanReport = True
        Else
            _Log.LogWarn("Bug reporting DLL is missing - unable to send reports.")
            MsgBox("You are missing a file, BugReport.dll - this file is needed in order to send bug reports and suggestions")
            Try
                Dim web_Download As New WebClient
                web_Download.DownloadFile("https://bitbucket.org/thegrandcoding/client/raw/HEAD/bin/Debug/BugReport.dll", "BugReport.dll")
            Catch ex As Exception
                _Log.LogError(ex.ToString())
            End Try
            btn_suggest.Enabled = False
        End If

        HandleOptions()
    End Sub

    Private Sub HandleOptions()
        ' Commandline allows user to override some settings that are in the registry.
        If File.Exists("commandline.txt") Then
            _Log.LogMsg("Command line found; loading options...")
            Dim allOptions As String() = File.ReadAllLines("commandline.txt")
            Dim lineNum As Integer = 0
            For Each line As String In allOptions
                lineNum += 1
                If String.IsNullOrEmpty(line) Then
                    _Log.LogWarn("Command line has invalid line at " & lineNum.ToString())
                    Continue For
                End If
                If Not line.Substring(0, 1) = "-" Then
                    _Log.LogError("Line " & lineNum.ToString() & " invalid, must start with '-'")
                    Continue For
                End If
                ' Handle the commands inputted..
                Dim command As String = line.Remove(0, 1) ' removes -
                ' format: [Option] [bool]
                Dim commandSplit As String() = command.Split(" ")
                If Not commandSplit.Length = 2 Then
                    _Log.LogError("[CL] Line " & lineNum.ToString() & " invalid; incorrect format.")
                    Continue For
                End If
                _Log.LogMsg("[CL] " & lineNum.ToString() & ": " & command)

                If commandSplit(0) = "forcename" Then
                    txt_Name.Text = commandSplit(1)
                    _Log.LogMsg("[CL] Name set via commandline")
                ElseIf commandSplit(0) = "forcelog" Then
                    DoLog = Boolean.Parse(commandSplit(1))
                    _Log.LogMsg("[CL] Logging set via commandline")
                ElseIf commandSplit(0) = "forceip" Then
                    connectIP = commandSplit(1)
                    _Log.LogMsg("[CL] IP set via commandline")
                ElseIf commandSplit(0) = "notify" Then
                    Notify = Boolean.Parse(commandSplit(1))
                ElseIf commandSplit(0) = "setchatmode" Then
                    ChatMode = commandSplit(1)
                    _Log.LogMsg("[CL] ChatMode set via commandline.")
                ElseIf commandSplit(0) = "forcereport" Then
                    ShouldReport = Boolean.Parse(commandSplit(1))
                    _Log.LogMsg("[CL] Report set via command line.")
                Else
                    _Log.LogError("[CL] Line " & lineNum.ToString() & " unknown option: " & commandSplit(0))
                End If
            Next
        Else
            _Log.LogMsg("[CL] No commandline, continuing.")
        End If
    End Sub

    Private Sub cmdMinimise_Click(sender As Object, e As EventArgs) Handles cmdMinimise.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Private Sub HandleBug()
        If ShouldReport = True Then
            BugReport.BugReporting.SendReport(_Log.SavePath, txt_Name.Text, "ChatProgram", "Bug_Report")
        End If
    End Sub
    Private Sub HandleClose(closeReason)
        Try
            ctThread.Abort()
        Catch ex As Exception
            ' nothing
        Finally
            ' we want to save a log file, when it closes
            _Log.SaveLog(closeReason)
            ' Bug reporting purposes.
            Try
                If CanReport = True Then
                    ' If we CAN report (ie, the .dll is there) 
                    HandleBug()
                End If
            Catch ex As Exception
                MsgBox(ex.ToString())
            End Try
        End Try
        Application.Exit()
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            SendMessage("/quit [closed:" + e.CloseReason.ToString + "]")
        Catch ex As Exception
            ' do nothing, just close.
        Finally
            HandleClose(e.CloseReason.ToString)
        End Try
    End Sub

    Private Sub flashTimer_Tick(sender As Object, e As EventArgs) Handles flashTimer.Tick
        If shouldFlash = True Then
            Dim res = WindowsApi.FlashWindow(Process.GetCurrentProcess().MainWindowHandle, True, True, 1)
        End If
    End Sub

    Private Sub buttonsTimer_Tick(sender As Object, e As EventArgs) Handles buttonsTimer.Tick
        If UserStruct.Connected = True Then
            UserStruct.Name = txt_Name.Text
            cmdConnect.Text = "Connected."
            cmdConnect.Enabled = False
            cmdChangeIP.Enabled = False
            txt_Name.Enabled = False
            txt_send.Enabled = True
            cmdSend.Enabled = True
        Else
            cmdConnect.Enabled = True
            cmdChangeIP.Enabled = True
            txt_Name.Enabled = True
            txt_send.Enabled = False
            cmdSend.Enabled = False
            cmdConnect.Text = "Connect to server"
        End If
        If String.IsNullOrEmpty(txt_send.Text) Then
            If HasSentIsTyping Then
                SendMessage("&NOTTYPING&", True)
                HasSentIsTyping = False
            End If
            Return
        End If
        If txt_send.Text.Length > 0 Then
            If txt_send.Text.Substring(0, 1) <> "/" Then
                If ChatMode = "Public" Then
                    ' only display is typing when it is not a command, 
                    ' and when it is being sent to public chat
                    If HasSentIsTyping = False Then
                        SendMessage("&ISTYPING&", True)
                        HasSentIsTyping = True
                    End If
                End If
            End If
        End If
    End Sub
    Dim versionNumber As Version
    Private Sub mainTextTimer_Tick(sender As Object, e As EventArgs) Handles mainTextTimer.Tick
        If UserStruct.Connected And UserStruct.LoggedIn Then
            Me.Text = versionNumber.ToString + ": Current IP: " + connectIP + " | Connected. | Logged in."
        ElseIf UserStruct.Connected Then
            Me.Text = versionNumber.ToString + ": Current IP: " + connectIP + " | Connected."
        Else
            Me.Text = versionNumber.ToString + ": Current IP: " + connectIP + " (Not Connected)"
        End If
        If UserStruct.LoggedIn Then
            Label1.Text = "Chat Mode: " + ChatMode
        Else
            Label1.Text = "Please enter your name: "
        End If
    End Sub

    Public Sub HandleNewPass()
        HandleHiddenSecrets("/changepass", "Please enter your current pass", "Please enter your new pass")
    End Sub

    Public Sub HandleHiddenSecrets(cmd As String, firstItem As String, secondItem As String, Optional title As String = "Client input.", Optional int As Integer = 2)
        Dim FirstValue As String
        Dim SecondValue As String = "err"
        Dim FirstPrompt As New PromptForm()
        FirstPrompt.SetMessage(firstItem, title, True)
        FirstPrompt.ShowDialog()
        FirstValue = FirstPrompt.Returned
        FirstPrompt.Dispose()
        If int = 2 Then
            Dim SecondPrompt As New PromptForm()
            SecondPrompt.SetMessage(secondItem, title, True)
            SecondPrompt.ShowDialog()
            SecondValue = SecondPrompt.Returned
            SecondPrompt.Dispose()
        End If
        If int = 2 Then
            SendMessage(cmd + " " + FirstValue + " " + SecondValue)
        Else
            SendMessage(cmd + " " + FirstValue)
        End If
    End Sub

    Private Sub lb_Chat_DoubleClick(sender As Object, e As EventArgs) Handles lb_Chat.DoubleClick
        Dim lvi As ListViewItem = Me.lb_Chat.SelectedItems(0)
        Dim displayItem As DisplayForm = New DisplayForm()
        displayItem.SetMessage(lvi.Text, "Text of clicked:")
        displayItem.ShowDialog(Me)
    End Sub

    Public HasSentIsTyping As Boolean = False

    Private Commands As New Dictionary(Of String, String)
    Private Sub PopulateCmds()
        If Commands.Count > 0 Then
            Return
        End If

        ' Server Commands  - Guest'
        Commands.Add("/register [password] [confirm password]", "Registers a new account")
        Commands.Add("/login [password]", "Log-in to a previously made account")
        ' Server Commands  - User'
        Commands.Add("/help", "Lists all server commands")
        Commands.Add("/players", "lists players online")
        Commands.Add("/admins", "lists all players online")
        Commands.Add("/pm [username] [message]", "private message")
        Commands.Add("/ask [question]", "ask a question to admins")
        Commands.Add("/sha256 [message]", "displays sha256 encrypt of message")
        Commands.Add("/dice", "rolls a fair 6-sided dice")
        Commands.Add("/changepass [password]", "change your password")
        Commands.Add("/setp [on/off]", "enables or disables public chat")
        Commands.Add("/quit", "disconnect peacefully")
        Commands.Add("/setpm [on/off]", "lets or prevents your private messages")
        Commands.Add("/shrug (message)", "displays ¯\_(ツ)_/¯ (after an optional message)")
        ' Server Commands  - Staff'
        Commands.Add("/broadcast [message]", "sends message to all&MOD&")
        Commands.Add("/a [message]", "sends message to all admins&MOD&")
        Commands.Add("/apm [user] [meassage]", "send an admin pm to a user.&MOD&")
        Commands.Add("/aduty", "toggles admin duty&MOD&")
        Commands.Add("/identity [user]", "displays info about user&MOD&")
        Commands.Add("/warn [user] [reason]", "warns user for given reason&MOD&")
        Commands.Add("/kick [user] [reason]", "kicks user for given reason&MOD&")
        Commands.Add("/alias [user]", "lists other accounts on the same computer&MOD&")
        Commands.Add("/see [user]", "see punishments made against a user.&MOD&")
        Commands.Add("/bans", "lists all bans&MOD&")
        Commands.Add("/notes [user]", "lists all notes against user.&MOD&")
        Commands.Add("/addnote [user] [message]", "adds a note against a user&MOD&")
        ' Client Commands '
        Commands.Add("/chatmode [public/admin/manager]", "Sets your chatmode to either public, admin or manager")
        Commands.Add("/notify [on/off]", "Toggles notifications (mentions will override)")
        Commands.Add("/clear", "Clears chat")
    End Sub

    Private Sub HandleToolTip(txt As String)
        PopulateCmds()
        ' Loop through command, check to see if txt could possibly be it.
        If String.IsNullOrWhiteSpace(txt) Then Return
        txtToolTip.Text = ""
        txtToolTip.Size = OriginalSize
        txtToolTip.Location = OriginalLocation
        For Each cmd As String In Commands.Keys
            Dim plauisbleText As String = ""
            Try
                plauisbleText = cmd.Substring(0, txt.Length)
            Catch ex As IndexOutOfRangeException
                Continue For
            Catch ex As ArgumentOutOfRangeException
                Continue For
            Catch ex As Exception
                _Log.LogError("At HTT: " & ex.ToString())
            End Try
            If plauisbleText = txt Then
                If Commands(cmd).Contains("&MOD&") Then
                    If UserStruct.Rank < 1 Then
                        Continue For
                    End If
                End If
                txtToolTip.SelectionFont = New Font(txtToolTip.Font, FontStyle.Bold)
                txtToolTip.AppendText(cmd)
                txtToolTip.SelectionFont = New Font(txtToolTip.Font, FontStyle.Regular)
                txtToolTip.AppendText(" - " + Commands(cmd).Replace("&MOD&", String.Empty) + vbCrLf)
            End If
        Next
        ' Loop through possible ones tooo
        txt = txt.Substring(1) ' removes /
        'For Each cmd As String In Commands.Keys
        '    If cmd.Contains(txt) Then
        '        If Not txtToolTip.Text.Contains(cmd) Then
        '            If Commands(cmd).Contains("&MOD&") Then
        '                If UserStruct.Rank < 1 Then
        '                    Continue For
        '                End If
        '            Else
        '                txtToolTip.SelectionFont = New Font(txtToolTip.Font, FontStyle.Bold)
        '                txtToolTip.AppendText(cmd)
        '                txtToolTip.SelectionFont = New Font(txtToolTip.Font, FontStyle.Regular)
        '                txtToolTip.AppendText(" - " + Commands(cmd).Replace("&MOD&", String.Empty) + vbCrLf)
        '            End If
        '        End If
        '    End If
        'Next
        If txtToolTip.Text = "" Then
            txtToolTip.Hide()
        Else
            txtToolTip.Show()
            If txtToolTip.Lines.Length > 1 Then
                ' Loop through for each command; increase size of txt for easier reading
                Dim _location As Point = txtToolTip.Location
                txtToolTip.Location = New Point(_location.X, _location.Y - (5 * txtToolTip.Lines.Length))
                Dim _size As Size = txtToolTip.Size
                txtToolTip.Size = New Size(_size.Width, _size.Height + (5 * txtToolTip.Lines.Length))
            End If
        End If
    End Sub

    Private OriginalSize As Size
    Private OriginalLocation As Point

    Private Sub txt_send_TextChanged(sender As Object, e As EventArgs) Handles txt_send.TextChanged
        If Not String.IsNullOrEmpty(txt_send.Text) Then
            If txt_send.Text = "/register" AndAlso UserStruct.LoggedIn = False Then
                txt_send.Enabled = False
                HandleHiddenSecrets("/register", "Please enter your password", "Please confirm your password", "Client registration")
                txt_send.Text = ""
                txt_send.Enabled = True
                txt_send.Focus()
            ElseIf txt_send.Text = "/changepass" AndAlso UserStruct.LoggedIn = True Then
                txt_send.Enabled = False
                HandleHiddenSecrets("/changepass", "Please enter your new password", "Please confirm", "Client change pass", 1)
                txt_send.Text = ""
                txt_send.Enabled = True
                txt_send.Focus()
            ElseIf txt_send.Text = "/login" AndAlso UserStruct.LoggedIn = False Then
                txt_send.Enabled = False
                HandleHiddenSecrets("/login", "Please enter your password", "Please confirm your password", "Client login", 1)
                txt_send.Text = ""
                txt_send.Enabled = True
                txt_send.Focus()
            ElseIf txt_send.Text = "/slogin" AndAlso UserStruct.LoggedIn = False Then
                txt_send.Enabled = False
                HandleHiddenSecrets("/slogin", "Please enter your password", "Please confirm your password", "Client login", 1)
                txt_send.Text = ""
                txt_send.Enabled = True
                txt_send.Focus()
            ElseIf txt_send.Text = "/notify off" Then
                Notify = False
                SetOption("Notifications", False)
                msg("Notification is off.", True)
                txt_send.Text = ""
            ElseIf txt_send.Text = "/notify on" Then
                Notify = True
                SetOption("Notifications", True)
                msg("Notification is on.", True)
                txt_send.Text = ""
            ElseIf (txt_send.Text = "/chatmode public" Or txt_send.Text = "/cm p") AndAlso UserStruct.LoggedIn = True Then
                ChatMode = "Public"
                msg("Chatmode is now public", True)
                txt_send.Text = ""
            ElseIf (txt_send.Text = "/chatmode admin" Or txt_send.Text = "/cm a") AndAlso UserStruct.LoggedIn = True Then
                ChatMode = "Admin Chat"
                msg("Chatmode is now /a", True)
                txt_send.Text = ""
            ElseIf (txt_send.Text = "/chatmode manager" Or txt_send.Text = "/cm m") AndAlso UserStruct.LoggedIn = True Then
                ChatMode = "Manager Chat"
                msg("Chatmode is now /m", True)
                txt_send.Text = ""
            ElseIf txt_send.Text = "/clear" Then
                lb_Chat.Items.Clear()
                msg("Chat has been cleared", True)
                txt_send.Text = ""
            Else
                If txt_send.Text.Split(" ").Length > 1 Then
                    ' The user has hit space after the command
                    txtToolTip.Hide()
                Else
                    If txt_send.Text.Substring(0, 1) = "/" Then
                        ' We dont do tool tips for non-commands!
                        HandleToolTip(txt_send.Text)
                    End If
                End If
            End If
        Else
            txtToolTip.Hide()
        End If
    End Sub

    Public Function GetOption(opt As String, defValue As Object) As Object
        Return My.Computer.Registry.GetValue(MainReg + "\Client", opt, defValue)
    End Function

    Public Sub SetOption(opt As String, value As Object)
        My.Computer.Registry.SetValue(MainReg + "\Client", opt, value)
        _Log.LogMsg("Created/Updated: " + MainReg + "\Client" + opt.ToString + "=" + value.ToString)
    End Sub

    Private Sub txt_send_KeyUp(sender As Object, e As KeyEventArgs) Handles txt_send.KeyUp
        If e.KeyValue = Keys.Up Then
            txt_send.Text = lastMessage
            txt_send.SelectionStart = txt_send.TextLength
        End If
    End Sub

    Private Sub cmdChangeIP_Click(sender As Object, e As EventArgs) Handles cmdChangeIP.Click
        IPHandle()
    End Sub

    Private Sub MainForm_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        shouldFlash = False 'This sub is called when the form is opened/selected (ie, the user looks at it)
        _isFocused = True
    End Sub

    Private Sub MainForm_Leave(sender As Object, e As EventArgs) Handles MyBase.Leave
        _isFocused = False
    End Sub

    Private Sub MainForm_Deactivate(sender As Object, e As EventArgs) Handles MyBase.Deactivate
        _isFocused = False
    End Sub

    Private Sub btnHelp_Click(sender As Object, e As EventArgs) Handles btnHelp.Click
        Dim helpPrompt As New DisplayForm()
        Dim str As String = "Hello!" + vbCrLf
        str += "This is a simple client for a chat program" + vbCrLf
        str += "You will first need the IP address of the server." + vbCrLf
        str += "In future versions, this will be done automatically." + vbCrLf + vbCrLf
        str += "Then you will need to /register or /login." + vbCrLf
        str += "Then you can talk to people! /players and /admins , use /ask for questions" + vbCrLf + vbCrLf
        str += "Available client commands: " + vbCrLf
        str += "/notify [on/off] - turns notifications off (will still notify if directly mentioned) " + vbCrLf
        str += "/chatmode [public/admin/manager] - turns your chat mode to public, admin or manager. Alias: /cm [p/a/m] " + vbCrLf
        str += "/clear - clears all messages in the chat " + vbCrLf
        helpPrompt.SetMessage(str, "Help information", "Help display", 2)
        helpPrompt.Show()
    End Sub

    Private Function ListAsString(lst As List(Of String), Optional seperator As String = " ") As String
        Dim str As String = ""
        For Each item As String In lst
            str += item + seperator
        Next
        Return str
    End Function

    Private Sub HandleServerCommand(cmd As String, args As List(Of String))
        Dim argsAsString As String = ListAsString(args)
        ' force new serial
        If cmd = "newserial" Then
            Try
                My.Computer.Registry.CurrentUser.DeleteSubKey("Alex_ChatProgram", True)
            Catch ex As System.ArgumentException
                _Log.LogWarn("Could not delete serial: it did not exist to begin with.")
            End Try
            HandleRegistryStuff()
            _Log.LogMsg("New serial generated on server request: " + UserStruct.Serial)
            CloseClient("5 This should not get through")
        End If
        If cmd = "quit" Then
            _Log.LogWarn("Closing on server request; reason: " & argsAsString)
            Try
                CloseClient("6 This should not get through")
            Catch _ex As Exception
            End Try
        End If
    End Sub
    Private TryReconnectAnyway As Boolean = False
    Private Sub ReConnectTimer_Tick(sender As Object, e As EventArgs) Handles ReConnectTimer.Tick
        If lastMessage = "/quit" Then
            ReConnectTimer.Stop()
            Return
        ElseIf secondLastMessage.Contains("[No-Reconnect]") Then
            ReConnectTimer.Stop()
            Return
        End If
        If HasConnected Or TryReconnectAnyway Then
            If IsTryingToConnect = False Then
                If UserStruct.Connected = False Then
                    cmdConnect.PerformClick()
                Else
                    ReConnectTimer.Interval = 5000
                    TryReconnectAnyway = False
                End If
            End If
        Else
            ReConnectTimer.Interval = 10000
            TryReconnectAnyway = True
        End If
    End Sub

    Private Sub UpdateSecondLastMsg()
        If Me.InvokeRequired Then
            Try
                Me.Invoke(Sub() UpdateSecondLastMsg())
            Catch ex As Exception
            End Try
            Return
        End If
        Try
            secondLastMessage = lb_Chat.Items(lb_Chat.Items.Count - 2).Text
        Catch ex As Exception
        End Try
    End Sub

    Public Sub CloseClient(reason As String)
        If String.IsNullOrEmpty(reason) Then reason = "Not given"
        Try
            SendMessage("/quit [CloseClient:" & reason & "]")
        Catch ex As Exception
        End Try
        clientSocket.Close()
    End Sub

    Private Sub testConnectionTimer_Tick(sender As Object, e As EventArgs) Handles testConnectionTimer.Tick
        'Try
        '    SendMessage("&IGNORE&", True)
        'Catch ex As Exception
        '    CloseClient("This should not get through")
        'End Try
    End Sub

    Private Sub btn_suggest_Click(sender As Object, e As EventArgs) Handles btn_suggest.Click
        Dim sugForm As New Suggestion
        sugForm.ShowDialog()
        Using sugForm ' Ensures it is disposed.
            If String.IsNullOrWhiteSpace(sugForm.Suggestion_Main) Then
                MsgBox("Your suggestion must contain text.. it won't be logged.")
                Return
            End If
            If String.IsNullOrWhiteSpace(sugForm.UserName) Then
                MsgBox("Your name can not be empty.. please try again")
                Return
            End If
            If String.IsNullOrWhiteSpace(sugForm.Concerns) Then
                MsgBox("Your topic ('Concerns') must be selected.. try again.")
                Return
            End If
            Dim info As String = "Topic: " & sugForm.Concerns & vbCrLf & "User: " & txt_Name.Text & vbCrLf
            BugReport.BugReporting.SendSuggestion(info + vbCrLf + sugForm.Suggestion_Main, sugForm.UserName, "ChatProgram", "None")
        End Using
    End Sub
End Class
