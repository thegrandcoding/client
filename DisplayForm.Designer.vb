﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DisplayForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblmsg = New System.Windows.Forms.Label()
        Me.tb_display = New System.Windows.Forms.RichTextBox()
        Me.btnZoomLess = New System.Windows.Forms.Button()
        Me.btnZoomMore = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblmsg
        '
        Me.lblmsg.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblmsg.Location = New System.Drawing.Point(0, 0)
        Me.lblmsg.Name = "lblmsg"
        Me.lblmsg.Size = New System.Drawing.Size(521, 23)
        Me.lblmsg.TabIndex = 0
        Me.lblmsg.Text = "DisplayMessage"
        '
        'tb_display
        '
        Me.tb_display.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tb_display.Location = New System.Drawing.Point(0, 23)
        Me.tb_display.Name = "tb_display"
        Me.tb_display.ReadOnly = True
        Me.tb_display.Size = New System.Drawing.Size(521, 300)
        Me.tb_display.TabIndex = 1
        Me.tb_display.Text = ""
        '
        'btnZoomLess
        '
        Me.btnZoomLess.Location = New System.Drawing.Point(488, 0)
        Me.btnZoomLess.Name = "btnZoomLess"
        Me.btnZoomLess.Size = New System.Drawing.Size(33, 23)
        Me.btnZoomLess.TabIndex = 2
        Me.btnZoomLess.Text = "-"
        Me.btnZoomLess.UseVisualStyleBackColor = True
        '
        'btnZoomMore
        '
        Me.btnZoomMore.Location = New System.Drawing.Point(449, 0)
        Me.btnZoomMore.Name = "btnZoomMore"
        Me.btnZoomMore.Size = New System.Drawing.Size(33, 23)
        Me.btnZoomMore.TabIndex = 3
        Me.btnZoomMore.Text = "+"
        Me.btnZoomMore.UseVisualStyleBackColor = True
        '
        'DisplayForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 323)
        Me.Controls.Add(Me.btnZoomMore)
        Me.Controls.Add(Me.btnZoomLess)
        Me.Controls.Add(Me.tb_display)
        Me.Controls.Add(Me.lblmsg)
        Me.Name = "DisplayForm"
        Me.Text = "DisplayForm"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblmsg As Label
    Friend WithEvents tb_display As RichTextBox
    Friend WithEvents btnZoomLess As Button
    Friend WithEvents btnZoomMore As Button
End Class
