﻿Public Class DisplayForm
    Private msg As String
    Private sMsg As String

    Private Sub DisplayForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblmsg.Text = sMsg
        tb_display.Text = msg
        btnZoomMore.Location = New Point(Me.Width - 70, 0)
        btnZoomLess.Location = New Point(Me.Width - 41, 0)
    End Sub

    Public Sub SetMessage(mainMessage As String, smallmessage As String, Optional title As String = "Client display", Optional size As Integer = 0)
        msg = mainMessage
        sMsg = smallmessage
        Me.Text = title
        While size > 0
            size -= 1
            UpdateFont(1)
        End While
        While size < 0
            size += 1
            UpdateFont(-1)
        End While
    End Sub

    Private Sub UpdateFont(amount As Single)
        Try
            Dim fnt As Font = tb_display.Font
            Dim newFnt As New Font(fnt.FontFamily, fnt.Size + amount, fnt.Style)
            tb_display.Font = newFnt
        Catch ex As Exception
            MsgBox("Error: text is too big or small")
        End Try
    End Sub

    Private Sub btnZoomMore_Click(sender As Object, e As EventArgs) Handles btnZoomMore.Click
        UpdateFont(1)
    End Sub

    Private Sub btnZoomLess_Click(sender As Object, e As EventArgs) Handles btnZoomLess.Click
        UpdateFont(-1)
    End Sub

    Private Sub DisplayForm_Resize(sender As Object, e As EventArgs) Handles MyBase.Resize
        btnZoomMore.Location = New Point(Me.Width - 70, 0)
        btnZoomLess.Location = New Point(Me.Width - 41, 0)
    End Sub
End Class