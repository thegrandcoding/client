﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PromptForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblmsg = New System.Windows.Forms.Label()
        Me.txt_Entry = New System.Windows.Forms.TextBox()
        Me.cmdEnter = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblmsg
        '
        Me.lblmsg.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblmsg.Location = New System.Drawing.Point(0, 0)
        Me.lblmsg.Name = "lblmsg"
        Me.lblmsg.Size = New System.Drawing.Size(482, 268)
        Me.lblmsg.TabIndex = 2
        Me.lblmsg.Text = "DisplayMessage"
        '
        'txt_Entry
        '
        Me.txt_Entry.Location = New System.Drawing.Point(12, 271)
        Me.txt_Entry.Name = "txt_Entry"
        Me.txt_Entry.Size = New System.Drawing.Size(377, 22)
        Me.txt_Entry.TabIndex = 0
        '
        'cmdEnter
        '
        Me.cmdEnter.Location = New System.Drawing.Point(395, 271)
        Me.cmdEnter.Name = "cmdEnter"
        Me.cmdEnter.Size = New System.Drawing.Size(75, 23)
        Me.cmdEnter.TabIndex = 3
        Me.cmdEnter.Text = "Enter"
        Me.cmdEnter.UseVisualStyleBackColor = True
        '
        'PromptForm
        '
        Me.AcceptButton = Me.cmdEnter
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(482, 306)
        Me.Controls.Add(Me.cmdEnter)
        Me.Controls.Add(Me.lblmsg)
        Me.Controls.Add(Me.txt_Entry)
        Me.Name = "PromptForm"
        Me.Text = "PromptForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents lblmsg As Label
    Private WithEvents txt_Entry As TextBox
    Private WithEvents cmdEnter As Button
End Class
