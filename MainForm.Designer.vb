<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cmdSend = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_send = New System.Windows.Forms.TextBox()
        Me.cmdConnect = New System.Windows.Forms.Button()
        Me.txt_Name = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.lb_Chat = New System.Windows.Forms.ListView()
        Me.Messages = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdMinimise = New System.Windows.Forms.Button()
        Me.mainTextTimer = New System.Windows.Forms.Timer(Me.components)
        Me.cmdChangeIP = New System.Windows.Forms.Button()
        Me.buttonsTimer = New System.Windows.Forms.Timer(Me.components)
        Me.flashTimer = New System.Windows.Forms.Timer(Me.components)
        Me.btnHelp = New System.Windows.Forms.Button()
        Me.ReConnectTimer = New System.Windows.Forms.Timer(Me.components)
        Me.lbl_CurTyping = New System.Windows.Forms.Label()
        Me.testConnectionTimer = New System.Windows.Forms.Timer(Me.components)
        Me.btn_suggest = New System.Windows.Forms.Button()
        Me.txtToolTip = New System.Windows.Forms.RichTextBox()
        Me.SuspendLayout()
        '
        'cmdSend
        '
        Me.cmdSend.Location = New System.Drawing.Point(304, 491)
        Me.cmdSend.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdSend.Name = "cmdSend"
        Me.cmdSend.Size = New System.Drawing.Size(143, 31)
        Me.cmdSend.TabIndex = 3
        Me.cmdSend.Text = "Send Message"
        Me.cmdSend.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(124, 14)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(152, 17)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Enter your chat name :"
        '
        'txt_send
        '
        Me.txt_send.AcceptsTab = True
        Me.txt_send.Location = New System.Drawing.Point(13, 440)
        Me.txt_send.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_send.Name = "txt_send"
        Me.txt_send.Size = New System.Drawing.Size(434, 22)
        Me.txt_send.TabIndex = 2
        '
        'cmdConnect
        '
        Me.cmdConnect.Location = New System.Drawing.Point(283, 42)
        Me.cmdConnect.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(164, 28)
        Me.cmdConnect.TabIndex = 1
        Me.cmdConnect.Text = "Connect to Server"
        Me.cmdConnect.UseVisualStyleBackColor = True
        '
        'txt_Name
        '
        Me.txt_Name.Location = New System.Drawing.Point(284, 11)
        Me.txt_Name.Margin = New System.Windows.Forms.Padding(4)
        Me.txt_Name.Name = "txt_Name"
        Me.txt_Name.Size = New System.Drawing.Size(163, 22)
        Me.txt_Name.TabIndex = 0
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(13, 42)
        Me.Button3.Margin = New System.Windows.Forms.Padding(4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(103, 28)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "Close"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'lb_Chat
        '
        Me.lb_Chat.Alignment = System.Windows.Forms.ListViewAlignment.Left
        Me.lb_Chat.AutoArrange = False
        Me.lb_Chat.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Messages})
        Me.lb_Chat.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lb_Chat.Location = New System.Drawing.Point(13, 77)
        Me.lb_Chat.Name = "lb_Chat"
        Me.lb_Chat.Size = New System.Drawing.Size(434, 356)
        Me.lb_Chat.TabIndex = 7
        Me.lb_Chat.UseCompatibleStateImageBehavior = False
        Me.lb_Chat.View = System.Windows.Forms.View.Details
        '
        'Messages
        '
        Me.Messages.Text = ""
        Me.Messages.Width = 326
        '
        'cmdMinimise
        '
        Me.cmdMinimise.Location = New System.Drawing.Point(13, 11)
        Me.cmdMinimise.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdMinimise.Name = "cmdMinimise"
        Me.cmdMinimise.Size = New System.Drawing.Size(103, 28)
        Me.cmdMinimise.TabIndex = 5
        Me.cmdMinimise.Text = "Minimise"
        Me.cmdMinimise.UseVisualStyleBackColor = True
        '
        'mainTextTimer
        '
        Me.mainTextTimer.Enabled = True
        Me.mainTextTimer.Interval = 1
        '
        'cmdChangeIP
        '
        Me.cmdChangeIP.Location = New System.Drawing.Point(124, 42)
        Me.cmdChangeIP.Margin = New System.Windows.Forms.Padding(4)
        Me.cmdChangeIP.Name = "cmdChangeIP"
        Me.cmdChangeIP.Size = New System.Drawing.Size(103, 28)
        Me.cmdChangeIP.TabIndex = 4
        Me.cmdChangeIP.Text = "Change IP"
        Me.cmdChangeIP.UseVisualStyleBackColor = True
        '
        'buttonsTimer
        '
        Me.buttonsTimer.Enabled = True
        Me.buttonsTimer.Interval = 1
        '
        'flashTimer
        '
        Me.flashTimer.Enabled = True
        Me.flashTimer.Interval = 1000
        '
        'btnHelp
        '
        Me.btnHelp.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnHelp.ForeColor = System.Drawing.Color.Maroon
        Me.btnHelp.Location = New System.Drawing.Point(13, 490)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(103, 31)
        Me.btnHelp.TabIndex = 10
        Me.btnHelp.Text = "Help!"
        Me.btnHelp.UseVisualStyleBackColor = False
        '
        'ReConnectTimer
        '
        Me.ReConnectTimer.Interval = 2000
        '
        'lbl_CurTyping
        '
        Me.lbl_CurTyping.AutoSize = True
        Me.lbl_CurTyping.Location = New System.Drawing.Point(12, 466)
        Me.lbl_CurTyping.Name = "lbl_CurTyping"
        Me.lbl_CurTyping.Size = New System.Drawing.Size(173, 17)
        Me.lbl_CurTyping.TabIndex = 11
        Me.lbl_CurTyping.Text = "No one is currently typing."
        '
        'testConnectionTimer
        '
        Me.testConnectionTimer.Enabled = True
        Me.testConnectionTimer.Interval = 1000
        '
        'btn_suggest
        '
        Me.btn_suggest.BackColor = System.Drawing.Color.Maroon
        Me.btn_suggest.ForeColor = System.Drawing.Color.AliceBlue
        Me.btn_suggest.Location = New System.Drawing.Point(122, 491)
        Me.btn_suggest.Name = "btn_suggest"
        Me.btn_suggest.Size = New System.Drawing.Size(175, 31)
        Me.btn_suggest.TabIndex = 12
        Me.btn_suggest.Text = "Suggestions"
        Me.btn_suggest.UseVisualStyleBackColor = False
        '
        'txtToolTip
        '
        Me.txtToolTip.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtToolTip.Location = New System.Drawing.Point(12, 410)
        Me.txtToolTip.Name = "txtToolTip"
        Me.txtToolTip.ReadOnly = True
        Me.txtToolTip.Size = New System.Drawing.Size(435, 32)
        Me.txtToolTip.TabIndex = 13
        Me.txtToolTip.Text = ""
        Me.txtToolTip.Visible = False
        '
        'MainForm
        '
        Me.AcceptButton = Me.cmdSend
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(460, 533)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtToolTip)
        Me.Controls.Add(Me.btn_suggest)
        Me.Controls.Add(Me.lbl_CurTyping)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.cmdChangeIP)
        Me.Controls.Add(Me.cmdMinimise)
        Me.Controls.Add(Me.lb_Chat)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.txt_Name)
        Me.Controls.Add(Me.cmdConnect)
        Me.Controls.Add(Me.txt_send)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdSend)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "MainForm"
        Me.Text = "Client"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdSend As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_send As System.Windows.Forms.TextBox
    Friend WithEvents cmdConnect As System.Windows.Forms.Button
    Friend WithEvents txt_Name As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents lb_Chat As ListView
    Friend WithEvents Messages As ColumnHeader
    Friend WithEvents cmdMinimise As Button
    Friend WithEvents mainTextTimer As Timer
    Friend WithEvents cmdChangeIP As Button
    Friend WithEvents buttonsTimer As Timer
    Friend WithEvents flashTimer As Timer
    Friend WithEvents btnHelp As Button
    Friend WithEvents ReConnectTimer As Timer
    Friend WithEvents lbl_CurTyping As Label
    Friend WithEvents testConnectionTimer As Timer
    Friend WithEvents btn_suggest As Button
    Friend WithEvents txtToolTip As RichTextBox
End Class
