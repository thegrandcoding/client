﻿Imports System.IO
Imports System.Net

Public Class IPPrompt
    Public SelectedIP As String
    Private Sub txt_Entry_TextChanged(sender As Object, e As EventArgs) Handles txtIP_1.TextChanged, txtIP_2.TextChanged, txtIP_3.TextChanged, txtIP_4.TextChanged
        Dim allowed As New List(Of Char) From {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
        Dim txt As String = sender.Text
        For Each char_ As Char In txt
            If Not allowed.Contains(char_) Then
                Dim ind As Integer = txt.IndexOf(char_)
                txt = txt.Remove(ind)
            End If
        Next
        sender.Text = txt
        UpdateLabel()
    End Sub

    Private Function IsValidIP(ip As String) As Boolean
        Dim result As Boolean = False
        Try
            Dim actualIP As Net.IPAddress = GetIPv4Address()
            result = Net.IPAddress.TryParse(ip, actualIP)
        Catch ex As Exception
            Return result
        End Try
        Return result
    End Function

    Private Sub UpdateLabel()
        Dim str = ""
        str += txtIP_1.Text + "."
        str += txtIP_2.Text + "."
        str += txtIP_3.Text + "."
        str += txtIP_4.Text
        lblIP.Text = str
    End Sub

    Private Function GetIPv4Address() As Net.IPAddress
        Dim strHostName As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(strHostName)

        For Each ipheal As System.Net.IPAddress In iphe.AddressList
            If ipheal.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                Return ipheal
            End If
        Next
        Return Nothing
    End Function

    Private firsttime As Boolean = True

    Private Sub IPPrompt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim storedIP As String = My.Computer.Registry.GetValue(MainForm.MainReg + "\Client", "DefaultIP", GetIPv4Address().ToString)
        Dim originalIPMsg As String = lblmsg.Text
        If storedIP Is Nothing Then storedIP = GetIPv4Address().ToString
        Dim splitIP As String() = storedIP.Split(".")
        txtIP_1.Text = splitIP(0)
        txtIP_2.Text = splitIP(1)
        txtIP_3.Text = splitIP(2)
        txtIP_4.Text = splitIP(3)
        Dim lastConnect As DateTime = DateTime.Parse(MainForm.GetOption("LastConnection", DateTime.Now()))
        If ((DateTime.Now() - lastConnect)).TotalDays >= 1 Then
            txtIP_4.Text = ""
            txtIP_3.Text = "" ' this can also be changed
            ' Remove final field due since last connect was last lesson
        End If
        UpdateLabel()
        ' Now we get the IP from bitbucket, if we can
        Try ' Get the IP on bitbucket (so we dont have to enter it in class)
            Dim myReq As HttpWebRequest = WebRequest.Create("https://bitbucket.org/thegrandcoding/server/raw/HEAD/Connection.txt")
            Dim response As HttpWebResponse = myReq.GetResponse()
            Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
            Dim firstLine As String = resStream.ReadLine()
            Dim firstLineSplit As String() = firstLine.Split(" ")
            Dim dateTimeOnline As String = firstLineSplit(1).Replace("_", " ").Replace("-", ":")
            Dim dateAsOnline As DateTime = DateTime.Parse(dateTimeOnline)
            Dim timeSpanDiff As TimeSpan = DateTime.Now() - dateAsOnline
            If timeSpanDiff.TotalHours < 3 Then
                Dim secondLine As String = resStream.ReadLine()
                secondLine = secondLine.Split(" ")(1)
                Dim ipSplit As String() = secondLine.Replace("_", ".").Split(".")
                txtIP_1.Text = ipSplit(0)
                txtIP_2.Text = ipSplit(1)
                txtIP_3.Text = ipSplit(2)
                txtIP_4.Text = ipSplit(3)
                MainForm._Log.LogMsg("IP address " & secondLine.Replace("_", ".") & " was from bitbucket")
                Me.cmdEnter.PerformClick()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString())
        End Try
        Dim mLThread As New Threading.Thread(AddressOf GetMasterList)
        mLThread.Start()
        lblMasterlist.Text = "Fetching masterlist..."
    End Sub

    Private MasterList As New Dictionary(Of String, MasterlistDLL.ClientDisplayServer)

    Private Sub GetMasterList()
        ' will use invokes inline to handle the datagrid view
        Dim allservers = MasterlistDLL.MasterListClient.GetServers(MasterlistDLL.MasterList_GameType.Alex_ChatServer)
        For Each serv As MasterlistDLL.ClientDisplayServer In allservers
            MasterList.Add(serv.ServerName, serv)
        Next
        Me.Invoke(Sub()
                      lblMasterlist.Text = "Populating.."
                      dgvMasterlist.Rows.Clear()
                      For Each serv As MasterlistDLL.ClientDisplayServer In MasterList.Values
                          Dim row() = New String() {serv.ServerName, serv.IPAddress}
                          dgvMasterlist.Rows.Add(row)
                      Next
                      lblMasterlist.Text = "Gotten " + MasterList.Values.Count.ToString() + " servers"
                  End Sub)
    End Sub

    Private Sub cmdEnter_Click(sender As Object, e As EventArgs) Handles cmdEnter.Click
        If IsValidIP(lblIP.Text) Then
            SelectedIP = lblIP.Text
            My.Computer.Registry.SetValue(MainForm.MainReg + "\Client", "DefaultIP", SelectedIP)
            Me.Close()
        Else
            MsgBox("Invalid IP address!")
        End If
    End Sub

    Private Sub IPPrompt_Activated(sender As Object, e As EventArgs) Handles MyBase.Activated
        If firsttime Then
            firsttime = False
            txtIP_4.Focus()
        End If
    End Sub

    Private Sub btnHelp_Click(sender As Object, e As EventArgs) Handles btnHelp.Click
        Dim helpPrompt As New DisplayForm
        helpPrompt.SetMessage("An IP address is how computers know where to send information." + vbCrLf + "You should be told the IP address to connect to by myself (Alex), if not, then I probably forgot ¯\_(ツ)_/¯" + vbCrLf + vbCrLf + "An IP consists of four numbers, between 1 and 255, seperated by a period (.)" + vbCrLf + "Examples: 1.1.1.1 ; 255.255.255.255 ; 129.168.1.192 .. and so on" + vbCrLf + vbCrLf + "THE THREE NUMBERS ALREADY IN THE BOXES MAY BE REMOVED: THIS IS YOUR COMPUTER'S IP ADDRESS AND IS PROBABLY NOT WHAT YOU NEED", "IP Help")
        helpPrompt.Show()
    End Sub
End Class